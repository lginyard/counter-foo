import React from "react";

/**
 * If count is 0 display "POOP_TEXT"
 * If count is divisible by 3 display "COUNTER_TEXT"
 * If count is divisible by 5 display "FOO_TEXT"
 * If count is divisible by 3 && 5 display "COUNTER_FOO_TEXT"
 * If count is not divisible by 3, 5 display "POOP_TEXT"
 * The increment button should increase the "count" variable
 * The decrement button should decrease the "count" variable
 * The "count" variable should never go below 0
 */

const COUNTER_TEXT = "Counter";
const FOO_TEXT = "Foo";
const COUNTER_FOO_TEXT = "🙅🏿‍♂️ Counter Foo 🙅🏿‍♂️";
const POOP_TEXT = "💩";

function App() {
  const [count, setCount] = React.useState(0);
  let text;

  if (count % 3 === 0) {
    text = COUNTER_TEXT;
  } else if (count % 5 === 0) {
    text = FOO_TEXT;
  } else if (count % 15 === 0) {
    text = COUNTER_FOO_TEXT;
  } else {
    text = POOP_TEXT;
  }

  return (
    <div style={styles.app}>
      <h1>{count}</h1>
      <h3>{text}</h3>
      <div style={styles.buttons}>
        <button onClick={() => setCount((c) => c + 1)}>Increment</button>
        <button onClick={() => setCount((c) => (c > 0 ? c - 1 : c))}>
          Decrement
        </button>
      </div>
    </div>
  );
}

const styles = {
  app: {
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    color: "skyBlue",
    backgroundColor: "darkBlue",
  },
  buttons: {
    display: "flex",
    flexDirection: "row",
  },
};
export default App;
